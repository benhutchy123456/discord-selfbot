package main

import (
	"strings"
	"time"

	"github.com/bwmarrin/discordgo"
)

var MuteCommand = Command{
	Name:        "mute",
	Description: "Adds user to mute list",
	Usage:       "mute <Mention> [Message]",
	Group:       "Trolling",
	Exec: func(m *discordgo.MessageCreate, c *Bot) {
		parts := strings.Split(strings.TrimPrefix(m.Content, c.Prefix), " ")
		if len(m.Mentions) == 0 {
			c.SendMessageEmbedTemp(m.ChannelID, ErrorEmbed("You need to mention one person"), 10)
			return
		}

		if m.Mentions[0].ID == c.Session.State.User.ID {
			return
		}

		message := "blocked!!!"
		if len(parts) > 2 {
			message = strings.Join(parts[2:], " ")
		}

		c.MutedPeople[m.GuildID] = make(map[string]*MutedUserInfo)
		c.MutedPeople[m.GuildID][m.Mentions[0].ID] = &MutedUserInfo{
			MuteMessage:         message,
			LastMuteMessageTime: time.Now().Unix(),
		}
	},
}

var UnmuteCommand = Command{
	Name:        "unmute",
	Description: "Removes user from mute list",
	Usage:       "mute <Mention>",
	Group:       "Trolling",
	Exec: func(m *discordgo.MessageCreate, c *Bot) {
		if len(m.Mentions) == 0 {
			c.SendMessageEmbedTemp(m.ChannelID, ErrorEmbed("You need to mention a blocked person"), 10)
			return
		}

		if _, exists := c.MutedPeople[m.GuildID]; exists {
			if _, exists := c.MutedPeople[m.GuildID][m.Mentions[0].ID]; exists {
				delete(c.MutedPeople[m.GuildID], m.Mentions[0].ID)
			}
		}
	},
}
