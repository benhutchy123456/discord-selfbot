package main

import (
	"fmt"
	"math"
	"time"

	"github.com/bwmarrin/discordgo"
)

var UptimeCommand = Command{
	Name:        "uptime",
	Description: "Get the uptime of the bot",
	Usage:       "uptime",
	Group:       "Utility",
	Exec: func(m *discordgo.MessageCreate, c *Bot) {
		executionTime := time.Now().Sub(StartingTime)

		timess := int(math.Round(executionTime.Seconds()))
		days := timess / 86400
		hours := (timess / 3600) - (days * 24)
		minutes := (timess / 60) - (days * 1440) - (hours * 60)
		seconds := timess % 60
		outputStr := fmt.Sprintf("%d Days, %02d Hours, %02d Minutes and %02d Seconds", days, hours, minutes, seconds)

		embed := discordgo.MessageEmbed{
			Title:       "Uptime",
			Description: outputStr,
			Color:       0xffaaaa,
		}

		c.Session.ChannelMessageSendEmbed(m.ChannelID, &embed)
	},
}
