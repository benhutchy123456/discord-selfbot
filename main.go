package main

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"os"
	"os/signal"
	"syscall"
	"time"

	"github.com/bwmarrin/discordgo"
)

var (
	cmdH         Bot
	StartingTime = time.Now()
)

type Config struct {
	Prefix string `json:"prefix"`
	Token  string `json:"token"`
}

func main() {
	config := Config{}
	data, err := ioutil.ReadFile("config.json")
	if err != nil {
		args := os.Args[1:]
		config.Token = args[0]
		config.Prefix = args[1]

	} else {
		err = json.Unmarshal(data, &config)
		if err != nil {
			panic(err)
		}
	}
	dg, err := discordgo.New(config.Token)
	if err != nil {
		fmt.Println("error creating Discord session,", err)
		return
	}
	cmdH = Bot{
		Categories:    make(map[string]*CommandCatagory),
		Prefix:        config.Prefix,
		RunningRepeat: make(map[string]chan bool),
		Session:       dg,
		MutedPeople:   make(map[string]map[string]*MutedUserInfo),
	}

	// NSFW
	cmdH.MakeCatagory(CommandCatagory{
		Name:        "NSFW",
		Description: "Don't tell mummy you've been here!!!",
		Commands:    make(map[string]*Command),
	})
	err = cmdH.Add(R34Command)
	// Trolling
	cmdH.MakeCatagory(CommandCatagory{
		Name:        "Trolling",
		Description: "We do a little bit of trolling",
		Commands:    make(map[string]*Command),
	})
	cmdH.Add(RepeatCommand)
	cmdH.Add(StopRepeatCommand)
	cmdH.Add(MuteCommand)
	cmdH.Add(UnmuteCommand)
	cmdH.Add(RespondCommand)
	cmdH.Add(StopRespondCommand)

	// Utility
	cmdH.MakeCatagory(CommandCatagory{
		Name:        "Utility",
		Description: "We, do a little bit of helping",
		Commands:    make(map[string]*Command),
	})
	cmdH.Add(UserinfoCommand)
	cmdH.Add(UptimeCommand)
	cmdH.Add(HelpCommand)
	cmdH.Add(DeleteCommand)
	// Fun
	cmdH.MakeCatagory(CommandCatagory{
		Name:        "Fun",
		Description: "damn bro having shit tonnes of fun with these commands",
		Commands:    make(map[string]*Command),
	})
	cmdH.Add(ReverseCommand)
	cmdH.Add(GoogleThatCommand)

	dg.AddHandler(messageCreate)

	err = dg.Open()
	if err != nil {
		fmt.Println("error opening connection,", err)
		return
	}

	fmt.Println("Bot is now running.  Press CTRL-C to exit.")
	sc := make(chan os.Signal, 1)
	signal.Notify(sc, syscall.SIGINT, syscall.SIGTERM, os.Interrupt, os.Kill)
	<-sc

	dg.Close()
}

func messageCreate(s *discordgo.Session, m *discordgo.MessageCreate) {
	cmdH.HandleCommand(s, m)
}
