package main

import (
	"github.com/bwmarrin/discordgo"
	"strings"
)

var StopRespondCommand = Command{
	Name:        "stoprespond",
	Description: "Stops responding to the mentioned person",
	Usage:       "stoprespond <mention>",
	Group:       "Trolling",
	Exec: func(m *discordgo.MessageCreate, c *Bot) {
		if len(m.Mentions) == 0 {
			c.SendMessageEmbedTemp(m.ChannelID, ErrorEmbed("No-one was mentioned"), 10)
			return
		}
		if _, e := c.RespondPeople[m.Mentions[0].ID]; !e {
			c.SendMessageEmbedTemp(m.ChannelID, ErrorEmbed("Not currently being responded to"), 10)
			return
		}
		delete(c.RespondPeople, m.Mentions[0].ID)
	},
}

var RespondCommand = Command{
	Name:        "respond",
	Description: "Send a reply everytime someone sends a message",
	Usage:       "respond <MENTION> <content>",
	Group:       "Trolling",
	Exec: func(m *discordgo.MessageCreate, c *Bot) {
		split := strings.Split(m.Content, " ")

		if len(split) < 3 {
			c.SendMessageEmbedTemp(m.ChannelID, ErrorEmbed("Not enough parameters"), 10)
			return
		}

		if len(m.Mentions) == 0 {
			c.SendMessageEmbedTemp(m.ChannelID, ErrorEmbed("No one mentioned"), 10)
			return
		}

		if c.RespondPeople == nil {
			c.RespondPeople = make(map[string]string)
		}

		c.RespondPeople[m.Mentions[0].ID] = strings.Join(split[2:], " ")
	},
}
