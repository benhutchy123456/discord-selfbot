package main

import (
	"strconv"
	"strings"
	"time"

	"github.com/bwmarrin/discordgo"
)

var ReverseCommand = Command{
	Name:        "reverse",
	Description: "Reverse the entered text",
	Usage:       "reverse <Message>",
	Group:       "Fun",
	Exec: func(m *discordgo.MessageCreate, c *Bot) {
		parts := strings.Split(strings.TrimPrefix(m.Content, c.Prefix), " ")
		if len(parts) == 1 {
			c.Session.ChannelMessageSendEmbed(m.ChannelID, ErrorEmbed("Needs like text to reverse retard"))
		}

		body := parts[1:]

		chars := strings.Split(strings.Join(body, " "), "")
		for i, j := 0, len(chars)-1; i < j; i, j = i+1, j-1 {
			chars[i], chars[j] = chars[j], chars[i]
		}

		c.Session.ChannelMessageSend(m.ChannelID, strings.Join(chars, ""))
	},
}

var GoogleThatCommand = Command{
	Name:        "googlethat",
	Description: "So much effort",
	Usage:       "googlethat <Search term>",
	Group:       "Fun",
	Exec: func(m *discordgo.MessageCreate, c *Bot) {

		parts := strings.Split(strings.TrimPrefix(m.Content, c.Prefix), " ")
		if len(parts) == 1 {
			c.Session.ChannelMessageSendEmbed(m.ChannelID, ErrorEmbed("Needs search term"))
			return
		}

		link := "http://letmegooglethat.com/?q="
		link += strings.Join(parts[1:], "+")
		c.Session.ChannelMessageSend(m.ChannelID, link)
	},
}

var DeleteCommand = Command{
	Name:        "delete",
	Description: "Delete a command after a set amount of time",
	Usage:       "delete <time> <message>",
	Group:       "Utility",
	Exec: func(m *discordgo.MessageCreate, c *Bot) {
		parts := strings.Split(m.Content, " ")
		duration, err := strconv.ParseFloat(parts[1], 64)
		if err != nil {
			c.SendMessageEmbedTemp(m.ChannelID, ErrorEmbed("No duration kid !!"), 10)
			return
		}

		msgContent := strings.Join(parts[2:], " ")
		msg, _ := c.Session.ChannelMessageSend(m.ChannelID, msgContent)
		length := time.Second
		length = time.Duration(float64(length) * duration)
		time.Sleep(length)
		c.Session.ChannelMessageDelete(m.ChannelID, msg.ID)
	},
}
