package main

import (
	"strings"

	"github.com/bwmarrin/discordgo"
)

var HelpCommand = Command{
	Name:        "help",
	Description: "Get a list of all commands or find more information",
	Usage:       "help [command]",
	Group:       "Utility",
	Exec: func(m *discordgo.MessageCreate, c *Bot) {
		parts := strings.Split(strings.TrimPrefix(m.Content, c.Prefix), " ")
		embed := discordgo.MessageEmbed{}
		if len(parts) == 2 {
			commandName := parts[1]
			for _, g := range c.Categories {
				for _, v := range g.Commands {
					if strings.ToLower(v.Name) == strings.ToLower(commandName) {
						embed.Title = v.Name
						embed.Footer = &discordgo.MessageEmbedFooter{
							Text: "[] Optional, <> Required",
						}
						embed.Color = 0xFFAA00
						fields := []*discordgo.MessageEmbedField{}
						fields = append(fields, &discordgo.MessageEmbedField{
							Name:  "Description",
							Value: v.Description,
						})

						fields = append(fields, &discordgo.MessageEmbedField{
							Name:  "Usage",
							Value: v.Usage,
						})
						embed.Fields = fields
					}
				}

			}
		} else {
			embed.Title = "List of Categories"
			embed.Description = "trollface lol !!!"
			embed.Color = 0xFFAA00
			fields := []*discordgo.MessageEmbedField{}
			for _, v := range c.Categories {
				fields = append(fields, &discordgo.MessageEmbedField{
					Name:   c.Prefix + v.Name,
					Value:  v.Description,
					Inline: true,
				})
			}

			embed.Fields = fields
			embed.Footer = &discordgo.MessageEmbedFooter{
				Text: "made by ya boi HutchyBen#1114",
			}
		}

		c.SendMessageEmbedTemp(m.ChannelID, &embed, 60)
	},
}
