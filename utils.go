package main

import (
	"fmt"
	"github.com/bwmarrin/discordgo"
	"strconv"
	"time"
)

func ErrorEmbed(description string) *discordgo.MessageEmbed {
	return &discordgo.MessageEmbed{
		Title:       "Uh oh error",
		Description: description,
		Color:       0xFF5555,
	}
}

func IDtoTime(id string) time.Time {
	// Convert to binary string
	bin, _ := strconv.ParseInt(id, 10, 64)
	strbin := strconv.FormatInt(bin, 2)

	// Do discord snowflake to get timstamp
	thing := len(strbin)
	binarytimestamp := strbin[0 : 42-(64-thing)]

	// Convert back to decimal
	decimalTimestamp, _ := strconv.ParseInt(binarytimestamp, 2, 64)
	godFuckMyLife, _ := strconv.Atoi(fmt.Sprintf("%d", decimalTimestamp))

	date := time.Unix(1420070400, 0)
	date = date.Add(time.Millisecond * time.Duration(godFuckMyLife))
	return date
}
