package main

import (
	"encoding/xml"
	"github.com/bwmarrin/discordgo"
	"io/ioutil"
	"math/rand"
	"net/http"
	"strconv"
	"strings"
)

type Posts struct {
	XMLName xml.Name `xml:"posts"`
	Text    string   `xml:",chardata"`
	Count   string   `xml:"count,attr"`
	Offset  string   `xml:"offset,attr"`
	Post    []struct {
		Text          string `xml:",chardata"`
		Height        string `xml:"height,attr"`
		Score         string `xml:"score,attr"`
		FileURL       string `xml:"file_url,attr"`
		ParentID      string `xml:"parent_id,attr"`
		SampleURL     string `xml:"sample_url,attr"`
		SampleWidth   string `xml:"sample_width,attr"`
		SampleHeight  string `xml:"sample_height,attr"`
		PreviewURL    string `xml:"preview_url,attr"`
		Rating        string `xml:"rating,attr"`
		Tags          string `xml:"tags,attr"`
		ID            string `xml:"id,attr"`
		Width         string `xml:"width,attr"`
		Change        string `xml:"change,attr"`
		Md5           string `xml:"md5,attr"`
		CreatorID     string `xml:"creator_id,attr"`
		HasChildren   string `xml:"has_children,attr"`
		CreatedAt     string `xml:"created_at,attr"`
		Status        string `xml:"status,attr"`
		Source        string `xml:"source,attr"`
		HasNotes      string `xml:"has_notes,attr"`
		HasComments   string `xml:"has_comments,attr"`
		PreviewWidth  string `xml:"preview_width,attr"`
		PreviewHeight string `xml:"preview_height,attr"`
	} `xml:"post"`
}

var R34Command = Command{
	Name:        "rule34",
	Description: "Gets images from rule34.xxx in groups of 5",
	Usage:       "rule34 <chunk num> <tags>",
	Group:       "NSFW",
	Exec: func(m *discordgo.MessageCreate, c *Bot) {
		split := strings.Split(m.Content, " ")
		if len(split) < 2 {
			c.SendMessageEmbedTemp(m.ChannelID, ErrorEmbed("Needs more than 1 parameter"), 10)
			return
		}

		url := "https://www.rule34.xxx/index.php?page=dapi&s=post&q=index&limit=5&json=0&tags="
		url += strings.Join(split[2:], "+")

		if split[1] == "rand" {
			data, err := http.Get(url)
			if err != nil {
				c.SendMessageEmbedTemp(m.ChannelID, ErrorEmbed("There was a problem with the web request"), 10)
				return
			}
			troll, err := ioutil.ReadAll(data.Body)

			var parsed Posts
			xml.Unmarshal(troll, &parsed)
			if len(troll) == 0 {
				c.SendMessageEmbedTemp(m.ChannelID, ErrorEmbed("There was no results?"), 10)
				return
			}
			if strings.Contains(string(troll), "API limited due to abuse") {
				c.SendMessageEmbedTemp(m.ChannelID, ErrorEmbed("Too high page"), 10)
				return
			}
			posts, err := strconv.Atoi(parsed.Count)
			if err != nil {
				c.SendMessageEmbedTemp(m.ChannelID, ErrorEmbed("API returned weird shit"), 10)
			}
			pages := posts / 5

			split[1] = strconv.Itoa(rand.Intn(pages))
		}

		url += "&pid=" + split[1]
		req, err := http.Get(url)
		if err != nil {
			c.SendMessageEmbedTemp(m.ChannelID, ErrorEmbed("There was a problem with the web request"), 10)
			return
		}
		data, _ := ioutil.ReadAll(req.Body)
		if len(data) == 0 {
			c.SendMessageEmbedTemp(m.ChannelID, ErrorEmbed("There was no results?"), 10)
			return
		}
		if strings.Contains(string(data), "API limited due to abuse") {
			c.SendMessageEmbedTemp(m.ChannelID, ErrorEmbed("Too high page"), 10)
			return
		}

		var parsed Posts
		xml.Unmarshal(data, &parsed)
		output := ""
		for _, v := range parsed.Post {
			output += v.FileURL + " "
		}

		c.Session.ChannelMessageSend(m.ChannelID, output)
	},
}
