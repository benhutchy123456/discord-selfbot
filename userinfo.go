package main

import (
	"log"
	"time"

	"github.com/bwmarrin/discordgo"
)

const POGFORMAT = "02-01-2006"

var UserinfoCommand = Command{
	Name:        "userinfo",
	Description: "Get info of a mentioned user",
	Group:       "Utility",
	Usage:       "userinfo <mention>",
	Exec: func(m *discordgo.MessageCreate, c *Bot) {
		if len(m.Mentions) == 0 {
			c.SendMessageEmbedTemp(m.ChannelID, ErrorEmbed("No users were mentioned"), 10)
			return
		}
		user := m.Mentions[0]
		nickname := user.Username
		joinedAt := time.Time{}
		if m.GuildID != "0" {
			member, err := c.Session.GuildMember(m.GuildID, user.ID)
			if member != nil && err != nil {
				nickname = member.Nick
				joinedAt, _ = member.JoinedAt.Parse()
			}

		}

		createdAt := IDtoTime(user.ID)

		embed := discordgo.MessageEmbed{
			Title: user.Username,
			Color: 0xFF5555,
			Image: &discordgo.MessageEmbedImage{
				URL: user.AvatarURL("1024"),
			},
			Fields: []*discordgo.MessageEmbedField{
				{
					Name:   "UserID",
					Value:  user.ID,
					Inline: true,
				},
				{
					Name:   "Nickname",
					Value:  nickname,
					Inline: true,
				},

				{
					Name:   "CreatedAt",
					Value:  createdAt.Format(POGFORMAT),
					Inline: true,
				},
			},
		}
		if !joinedAt.IsZero() {
			embed.Fields = append(embed.Fields, &discordgo.MessageEmbedField{
				Name:   "JoinedAt",
				Value:  joinedAt.Format(POGFORMAT),
				Inline: true,
			})
		}
		_, err := c.Session.ChannelMessageSendEmbed(m.ChannelID, &embed)
		if err != nil {
			log.Println(err)
		}
	},
}
