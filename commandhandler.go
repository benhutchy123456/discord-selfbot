package main

import (
	"errors"
	"github.com/bwmarrin/discordgo"
	"log"
	"strings"
	"time"
)

type CommandExec func(m *discordgo.MessageCreate, c *Bot)

type Command struct {
	Name        string
	Description string
	Usage       string
	Exec        CommandExec
	Group       string
}

type CommandCatagory struct {
	Name        string
	Description string
	Commands    map[string]*Command
}

type MutedUserInfo struct {
	MuteMessage         string
	LastMuteMessageTime int64
}

type Bot struct {
	Categories    map[string]*CommandCatagory
	Prefix        string
	RunningRepeat map[string]chan bool
	Session       *discordgo.Session
	MutedPeople   map[string]map[string]*MutedUserInfo // map[guildid]map[userid]kekmeassage
	RespondPeople map[string]string                    //map[userid]content
}

func (c *Bot) MakeCatagory(group CommandCatagory) {
	if _, e := c.Categories[group.Name]; e {
		return
	}

	c.Categories[strings.ToLower(group.Name)] = &group

}

func (c *Bot) Add(cmd Command) error {
	if _, e := c.Categories[strings.ToLower(cmd.Group)]; !e {
		return errors.New("group does not exist")
	}

	for _, v := range c.Categories {
		for _, v := range v.Commands {
			if cmd.Name == v.Name {
				return errors.New("command already exists")
			}
		}
	}

	c.Categories[strings.ToLower(cmd.Group)].Commands[strings.ToLower(cmd.Name)] = &cmd
	return nil
}

func (c *Bot) HandleMuted(m *discordgo.MessageCreate) {
	if v, exist := c.MutedPeople[m.GuildID]; exist {
		value, existance := v[m.Author.ID]
		if existance {
			c.Session.ChannelMessageDelete(m.ChannelID, m.ID)
			if time.Now().Unix() > value.LastMuteMessageTime {
				c.Session.ChannelMessageSend(m.ChannelID, value.MuteMessage)
				value.LastMuteMessageTime = time.Now().Unix() + 10
			}
		}
	}
}

func (c *Bot) HandleResponds(m *discordgo.MessageCreate) {
	if v, exist := c.RespondPeople[m.Author.ID]; exist {

		c.Session.ChannelMessageSendReply(m.ChannelID, v, &discordgo.MessageReference{
			MessageID: m.ID,
			ChannelID: m.ChannelID,
			GuildID:   m.GuildID,
		})
	}
}

func (c *Bot) HandleCommand(s *discordgo.Session, m *discordgo.MessageCreate) {

	c.HandleMuted(m)
	c.HandleResponds(m)

	if m.Author.ID != s.State.User.ID {
		return
	}

	if !strings.HasPrefix(m.Content, c.Prefix) {
		return
	}

	err := s.ChannelMessageDelete(m.ChannelID, m.ID)
	if err != nil {
		log.Fatal(err)
	}

	command := strings.TrimPrefix(m.Content, c.Prefix)
	commandName := strings.ToLower(strings.Split(command, " ")[0])

	for _, cate := range c.Categories {
		if commandName == strings.ToLower(cate.Name) {
			// Get all the commands and put into list of fields
			fields := []*discordgo.MessageEmbedField{}
			for _, cmd := range cate.Commands {
				fields = append(fields, &discordgo.MessageEmbedField{
					Name:   cmd.Name,
					Value:  cmd.Description,
					Inline: true,
				})

			}

			embed := discordgo.MessageEmbed{
				Title:  "List of Commands",
				Fields: fields,
			}

			c.SendMessageEmbedTemp(m.ChannelID, &embed, 60)
			return

		}
		// Check for commands
		if v, e := cate.Commands[commandName]; e {
			v.Exec(m, c)
		}
	}
}

// Function that deletes messages after set amount of time
func (c *Bot) SendMessageTemp(cID string, content string, seconds int) {
	msg, err := c.Session.ChannelMessageSend(cID, content)
	if msg == nil || err != nil {
		return
	}
	go func() {
		time.Sleep(time.Second * time.Duration(seconds))
		c.Session.ChannelMessageDelete(msg.ChannelID, msg.ID)
	}()
}

func (c *Bot) SendMessageEmbedTemp(cID string, content *discordgo.MessageEmbed, seconds int) {
	msg, err := c.Session.ChannelMessageSendEmbed(cID, content)
	if msg == nil || err != nil {
		return
	}
	go func() {
		time.Sleep(time.Second * time.Duration(seconds))
		c.Session.ChannelMessageDelete(msg.ChannelID, msg.ID)
	}()
}
