package main

import (
	"log"
	"math"
	"strconv"
	"strings"
	"time"

	"github.com/bwmarrin/discordgo"
)

var StopRepeatCommand = Command{
	Name:        "stoprepeat",
	Usage:       "stoprepeat",
	Description: "Stop a repeating in the current channel",
	Group:       "Trolling",
	Exec: func(m *discordgo.MessageCreate, c *Bot) {
		if _, exist := c.RunningRepeat[m.ChannelID]; !exist {
			return
		}

		c.RunningRepeat[m.ChannelID] <- true
	},
}

var RepeatCommand = Command{
	Name:        "repeat",
	Description: "Repeat text for a set amount of time",
	Usage:       "repeat <Seconds> <Message>",
	Group:       "Trolling",
	Exec: func(m *discordgo.MessageCreate, c *Bot) {
		parts := strings.Split(strings.TrimPrefix(m.Content, c.Prefix), " ")
		duration, err := strconv.ParseFloat(parts[1], 64)
		if err != nil {
			c.SendMessageEmbedTemp(m.ChannelID, ErrorEmbed("The duration is not a int"), 10)
			return
		}

		length := time.Second
		length = time.Duration(math.Floor(float64(length) * duration))
		go runRepeat(m, c, length)
	},
}

func runRepeat(m *discordgo.MessageCreate, ch *Bot, timeBetween time.Duration) {
	ch.RunningRepeat[m.ChannelID] = make(chan bool)
	parts := strings.Split(strings.TrimPrefix(m.Content, ch.Prefix), " ")
	if timeBetween == time.Duration(0) {
		timeBetween = time.Duration(1)
	}
	ticker := time.NewTicker(timeBetween)
	for {
		log.Println("Attempting send message")
		_, err := ch.Session.ChannelMessageSend(m.ChannelID, strings.Join(parts[2:], " "))
		if err != nil {
			log.Println("Could not send messsage: " + err.Error())

		}
		select {
		case <-ch.RunningRepeat[m.ChannelID]:
			log.Println("Stopping repeat in " + m.ChannelID)
			return
		case <-ticker.C:
		}
	}
}
